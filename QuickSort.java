import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

/**
 * Created by estebes on 01-05-2016.
 */
public class Test {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int lines = Integer.parseInt(bufferedReader.readLine().split(" ")[0]);

        while(lines > 0) {
            String[] inputData = bufferedReader.readLine().split(" ");
            int[] parsedData = new int[inputData.length];
            for (int i = 0; i < inputData.length; i++) {
                parsedData[i] = Integer.parseInt(inputData[i]);
            }
            queue.add(new Node(parsedData));
            lines--;
        }

        long startTime = System.nanoTime();
        performQuickSort(0, collection.size() - 1);
        formattedOutput();
        long stopTime = System.nanoTime();
        System.out.println(stopTime - startTime);
    }

    /**
     * QuickSort implementation
     * @param first Index for the first element.
     * @param last Index for the last.
     */
    public static void performQuickSort(int first, int last) {
        Node pivot = collection.get(first);
        int leftMarker = first + 1, rightMarker = last;

        while(leftMarker <= rightMarker) {
            while(collection.get(leftMarker).compareTo(pivot) < 0 && leftMarker < rightMarker) {
                leftMarker++;
            }

            while(collection.get(rightMarker).compareTo(pivot) > 0) {
                rightMarker--;
            }

            if (leftMarker <= rightMarker) {
                Node temp = collection.get(leftMarker);
                collection.set(leftMarker, collection.get(rightMarker));
                collection.set(rightMarker, temp);
                leftMarker++;
                rightMarker--;
            }
        }

        int splitPoint = leftMarker - 1;
        collection.set(first, collection.get(splitPoint));
        collection.set(splitPoint, pivot);

        if(splitPoint - 1 > first) performQuickSort(first, splitPoint - 1);
        if(splitPoint + 1 < last) performQuickSort(splitPoint + 1, last);
    }

    /**
     * Formatted Output
     */
    public static void formattedOutput() {
        StringBuilder ret = new StringBuilder();
        for (Iterator<Node> it = collection.iterator(); it.hasNext();) {
            ret.append(it.next());
            if(it.hasNext()) ret.append(System.getProperty("line.separator"));
        }
        System.out.println(ret.toString());
    }

    /**
     * Vars
     */
    static List<Node> collection = new ArrayList<Node>();
    static Queue<Node> queue = new ArrayDeque<Node>();
}

/**
 * Node Class
 * Implements {@link java.lang.Comparable Comparable} and overrides {@link java.lang.Comparable#compareTo(Object) compareTo}
 * to simplify the QuickSort operation.
 */
final class Node implements Comparable<Node> {
    /**
     * Node Constructor
     * @param dimensions Array with the dimensions.
     */
    public Node(int... dimensions){
        if (dimensions.length < 1) throw new IllegalArgumentException();
        this.dimensions = dimensions;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        for (int aux = 0; aux < dimensions.length; aux++) {
            ret.append(dimensions[aux]);
            if (aux < dimensions.length - 1) ret.append(" ");
        }
        return ret.toString();
    }

    @Override
    public int compareTo(Node node) {
        if (this.dimensions.length > node.dimensions.length) {
            return 1;
        } else if (this.dimensions.length < node.dimensions.length) {
            return -1;
        } else {
            for (int i = 0; i < this.dimensions.length; i++) {
                if (this.dimensions[i] > node.dimensions[i]) {
                    return 1;
                } else if (this.dimensions[i] < node.dimensions[i]) {
                    return -1;
                }
            }
        }

        return 0;
    }

    /**
     * Immutable int array.
     */
    final int[] dimensions;
}
